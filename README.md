----       Projet Ada-Pacman:

1 -- Build et execution
  Ce projet est développé en Ada. Il repose sur la bibliothèque GNAT disponible sur le site officiel et de la bibliothèque graphique Gtkada également disponible et instalable depuis le site D'AdaCore.
  Le compilateur GNAT fournit avec la bibliothèqe sera utilisé pour compiler se projet ainsi que la commande gprbuild utilisée pour générer un executable à partir de groupes d'instructions spécifiques présents dans un fichier .gpr.
  Ainsi, depuis la racine du projet, construire le fichier pacman.gpr avec la commande "gprbuild pacman.gpr".
  L'execution se fait par l'appel au main généré dans le dossier /obj/ depuis la racine "./obj/main".
  Le projet est également compatible avec GnatStudio.

2 -- Manuel d'utilisation
  Le projet Ada-Pacman est un projet qui permet de jouer au jeu de pacman. Au lancement, le joueur se trouve sur une carte où il devra échaper au phantômes et manger des bulles jaunes. Il accumulera ainsi un score au travers de différents étage, d'objets et de certaines fonctionnalités.

3 -- Software Requierement
  High-Level Requirement:

    - 1. "Le terrain du jeu" :
     La carte devra être visualisable par le joueur.
     Chaques éléments de la carte devront commencer à des positions spécifiques et suivre un comportement prédéfinit.
     La carte doit être mise à jour périodiquement.
     La carte devra imposer au joueur des règles de déplacement spécifique.

    - 2. "Le personnage" : 
     Le personnage doit toujours avancer si il le peut selon les règles de la carte.
     Le joueur doit pouvoir modifier le déplaclement du personnage.
     Le personnage doit respecter les évènements de chaques objets rencontrés.
     Le personnage peut se retrouver dans un état de fuite ou dans un état de puissance.

    - 3. "Les ia" : 
     Les ia doivent pouvoir se déplacer de façon autonomes.
     Les ia doivent respecter les contraintes de la carte.
     Les ia doivent respecter les contraintes de vitesse qui leur sont assignées.
     Lors d'une rencontre avec le personnage, l'Ia et le personnage doivent suivre le comportement attendu par le status actuel du personnage.

  Low-Level Requirement:
     - la fonction On_Draw_Cb devra être rappelée périodiquement. (1.3)
     - la fonction On_Draw_Cb devra pouvoir montrer un résultat différent d'un appel précédent.(1.1 et 1.2)
     - la fonction Check_Content devra déterminer la présence ou non d'un unique objet en un point de la map.(1.1)
     - L'objet player ne pourra avoir qu'un seul statut possible en un instant t.(2.4)
     - La fonction Move devra respecter les contraintes de la carte.(2.3 et 3)
     - La fonction Move devra respecter les évenéments des objets présents sur la carte. ((2.3)

4 -- Architecture
  dossier obj : objets de construction du binaire
  dossier ressource : éléments graphiques utilisés dans le jeu
  dossier src : fichiers sources du projet:
      Le jeu est organisé en différent packages ayant chacuns leurs propres comportements et attributs;
         - Le terrain de jeu "game_view.ads", qui regroupe la carte et le score. Il est chargé d'aficher le terrain et gérer les interactions entre les différents éléments s'y trouvant.
         - Le joueur "player.ads", qui avance en continue et dont l'utilisateur peu modifier le mouvement. Il contiend ses déplacements, ses vies, ses objets, sa vitesse, son statut, sa position et son orientation.
         - Les Ias "ia.ads", non implémenté, qui tournent autours du joueur selon une routine spécifique propre afin de le manger. Elles contiennent leur valeur, leur position, leur status et leur orientation.
         - Les objets statiques "object.ads", qui enclenche un évènement spécifique pouvant aller de changement de points à modification des status sur la carte.

