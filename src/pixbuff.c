#include <glib.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <stdio.h>
#include "pixbuff.h"
	
int
get_pixel (guchar *pixels, int x, int y,  int wh, int s, int rowstride, int n_channels, int index)
{
  guchar *p;

  p = pixels + y * rowstride + x * n_channels;
  return p[index];
}

