with Ada.Text_IO; use Ada.Text_IO;
with Gdk.Event;       use Gdk.Event;

with Gtk.Box;         use Gtk.Box;
with Gtk.Label;       use Gtk.Label;
with Gtk.Widget;      use Gtk.Widget;
with Gtk.Main;
with Gtk.Image;       use Gtk.Image;
with Gtk.Window;      use Gtk.Window;
with Ada.Directories; use Ada.Directories;
with Gdk.Pixbuf; use Gdk.Pixbuf;
with Glib.Error; use Glib.Error;

with Gtk.Enums; use Gtk.Enums;
with Gtkada.Canvas; use Gtkada.Canvas;
with Gtk.Frame; use Gtk.Frame;
with Game_View; use Game_View;


procedure Main is

   Win   : Gtk_Window;
   Game_Space  : Game_Access;


   function Delete_Event_Cb
     (Self  : access Gtk_Widget_Record'Class;
      Event : Gdk.Event.Gdk_Event)
      return Boolean;

   ---------------------
   -- Delete_Event_Cb --
   ---------------------

   function Delete_Event_Cb
     (Self  : access Gtk_Widget_Record'Class;
      Event : Gdk.Event.Gdk_Event)
      return Boolean
   is
      pragma Unreferenced (Self, Event);
   begin
      Gtk.Main.Main_Quit;
      return True;
   end Delete_Event_Cb;

begin
   --  Initialize GtkAda.
   Gtk.Main.Init;

   --  Create a window with a size of 400x400
   Gtk_New (Win);
   Win.Set_Size_Request(800, 950);

   --  Create a box to organize vertically the contents of the window

   Game_Space := Initialise(Win);
   --Game_View.Draw_Initial_Map(Win, Box, Game_Space);
   --Game_View.Redraw_On_Callback(Win, Box, Game_Space);
   -- Stop the Gtk process when closing the window
   Win.On_Delete_Event (Delete_Event_Cb'Unrestricted_Access);

   --  Show the window and present it
   Win.Show_All;

   --  Start the Gtk+ main loop
   Gtk.Main.Main;
end Main;
