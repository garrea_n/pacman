with Gtk.Box; use Gtk.Box;
with Gtk.Window; use Gtk.Window;
with Gdk.Pixbuf; use Gdk.Pixbuf;
with Gtk.Image; use Gtk.Image;
with Gtk.Drawing_Area; use Gtk.Drawing_Area;
with Cairo; use Cairo;
with Gdk; use Gdk;
with Gtk.Frame; use Gtk.Frame;
with Glib.Object; use Glib.Object;
with player;


package Game_View with SPARK_Mode =>On is
   type Mode is (Init, In_Game, End_Game);
   type Content is (Empty, Bubble, Wall, Ghost);

   Bullet_Number : Integer := 100; --to be modified

   type Game is new GObject_Record with
      record
        Game_Mode               : Mode;
        Score                   : Integer;
        Box_Score               : Gtk_HBox;
        Box_Game                : Gtk_Hbox;
        Box_Draw                : Gtk_Drawing_Area;
        Box_Items               : Gtk_Hbox;
        Actual_Map              : Gdk_Pixbuf;
        Player_c                : Player.Player;
      end record;
   type Game_Access is access all Game'Class;

   function Initialise(Win : in Gtk_Window) Return Game_Access with
     Pre => Win /= null,
     Post => Initialise'Result /= null;

   function On_Draw_Cb(Gobj : access GObject_Record'Class; Cr : Cairo_Context) return Boolean;

   function Check_Content(X : in Integer; Y: in Integer; Map : in Gdk_Pixbuf) Return Content with
     Pre => X > 0 and X <= 28 and Y > 0 and Y <= 31;

   procedure Draw_Bubble(X : in Integer; Y : in Integer; Map : in out Gdk_Pixbuf) with
     Pre => X > 0 and X <= 28 and Y > 0 and Y <= 31;


   function Move(Player_c : in out Player.Player; Game_space : in out Game)  return
     Game_View.Content with
       Pre => Player_c.Life > 0;

   procedure Draw_Player(Player_c : in Player.Player; Map : in out Gdk_Pixbuf);

end Game_View;
