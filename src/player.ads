with Gdk.Pixbuf; use Gdk.Pixbuf;

package player with SPARK_Mode =>On  is
   
   type Mode is (Running, Pursuing);
   type Action is (None, Up, Down, Left, Right);
   
   type Position is
      record
         X : Integer range 1 .. 28;
         Y : Integer range 1 .. 31;
      end record;
   
 type Player is
     record
        Life                     : Integer;
        Player_Mode              : Mode;
        Orientation              : Action;
        Pos                      : Position;
        Vitesse                  : Integer;
     end record;
   
   function Initialise Return Player;

end player;
