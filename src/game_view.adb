with Ada.Directories;
with Ada.Text_IO; use Ada.Text_IO;
with Gdk.Pixbuf; use Gdk.Pixbuf;
with Gtk.Image; use Gtk.Image;
with Glib.Error; use Glib.Error;
with Gtk.Label; use Gtk.Label;
with Ada; use Ada;
with Glib; use Glib;
with Gtk.Box; use Gtk.Box;
with Gtk.Widget; use Gtk.Widget;
with Gdk.Color; use Gdk.Color; use Gdk.Color;
with Gtk.Enums; use Gtk.Enums;
with Gdk.RGBA; use Gdk.RGBA;
with System; use System;
with System.Address_To_Access_Conversions;
with Cairo; use Cairo;
with Gdk.Cairo; use Gdk.Cairo;
with Interfaces.C; use Interfaces.C;
with Ada.Real_Time; use Ada.Real_Time;
with player; use player;


package body Game_View is

   function Initialise(Win : in Gtk_Window) Return Game_Access is
      Box : Gtk_Vbox;
      Box_Score : Gtk_Hbox;
      Box_Game : Gtk_Hbox;
      Box_Draw :Gtk_Drawing_Area;
      Box_Items : Gtk_Hbox;
      Actual_Map : Gdk_Pixbuf;

      Game_Space : Game_Access;

      Error : GError;
      Black : Gdk_Color;
      Player_s : PLayer.Player;
   begin
      Set_Rgb(Black, 0,0,0);
      Gdk_New_From_File(Actual_Map, "resource/field.png", Error);

      Gtk_New_Vbox(Box);
      Win.Add(Box);

      Gtk_New_Hbox(Box_Score);
      Box_Score.Modify_Bg(State => State_Normal, Color => Black);
      Box_Score.Set_Size_Request(800, 75);
      Box.Add(Box_Score);



      Gtk_New_Hbox(Box_Game, Homogeneous => False);
      Box_Game.Modify_Bg(State => State_Normal, Color => Black);
      Box.Add(Box_Game);
      Gtk_New(Box_Draw);
      Box_Game.Pack_Start(Child   => Box_Draw,
                          Expand  => True,
                          Fill    => True);


      Gtk_New_Hbox(Box_Items);
      Box_Items.Modify_Bg(State => State_Normal, Color => Black);
      Box_Items.Set_Size_Request(800, 75);
      Box.Add(Box_Items);
      Player_s := Player.Initialise;


      Game_Space :=  new Game;
      Game_Space.Score := 0;
      Game_Space.Box_Score := Box_Score;
      Game_Space.Box_Game := Box_Game;
      Game_Space.Box_Items := Box_Items;
      Game_Space.Actual_Map := Actual_Map;
      Game_Space.Player_c := Player_s;
      Game_Space.Box_Draw := Box_Draw;


      Box_Draw.On_Draw(On_Draw_Cb'Access, Game_Space);

      Return Game_Space;

   end Initialise;

   function On_Draw_Cb(Gobj : access GObject_Record'Class; Cr : Cairo_Context) return Boolean is
      Game_Space : aliased Game_Access := Game_Access(Gobj);
      Map_rescaled : Gdk_Pixbuf;
      Action : Mode;

   begin
      Action := Game_Space.Game_Mode;
      case Action is
      when Init =>

      Draw_Player(Game_Space.Player_c, Game_Space.Actual_Map);

      for X in 1..28 loop
          for Y in 1..31 loop
            if Check_Content(X, Y,  Game_Space.Actual_Map) = Empty then
               Draw_Bubble(X, Y,  Game_Space.Actual_Map);
            end if;
          end loop;
      end loop;

      Gdk.Cairo.Set_Source_Pixbuf(Cr       => Cr,
                                  Pixbuf   => Game_Space.Actual_Map,
                                  Pixbuf_X => GDouble(0),
                                  Pixbuf_Y => Gdouble(0));

      Map_rescaled := Gdk.Pixbuf.Scale_Simple(Src =>  Game_Space.Actual_Map,
                                           Dest_Width  => 800,
                                              Dest_Height => 800);
       Game_Space.Game_Mode := In_Game;
      when In_Game =>
            --persos calculate
            --persos replace empty tile behind
            --save image

            --persos place

            --ia calculate
            --ia place

         null;
      when End_Game =>
            --Win
            --Menu?
            --Score

         null;
      end case;
      Cairo.Fill(Cr);
      Return False;


   end On_Draw_Cb;

   function Check_Content(X : in Integer; Y: in Integer; Map : in Gdk_Pixbuf) Return Content is
      Pixels : Rgb_Buffer_Access ;
      W : Integer;
      H : Integer;
      S : Integer := 43;
      Res : Integer;

      function Get_Pixel(Pixels : Rgb_Buffer_Access; X : Integer; Y : Integer; Wh : Integer; S : Integer; Row : Integer; Channel : Integer; Index : Integer) return Integer
        with
          Import => True,
          Convention => C,
          Pre => Gdk.Pixbuf.Get_Colorspace(Map) = gdk.Pixbuf.Colorspace_RGB
        and Gdk.Pixbuf.Get_Bits_Per_Sample(Map) = 8
        and Gdk.Pixbuf.Get_Has_Alpha(Map)
        and Channel = 4,
          Post => Get_Pixel'Result <= 256 and Get_Pixel'Result >= 0;

   begin
      Pixels := Gdk.Pixbuf.Get_Pixels(Map);
      for WH in 1..S loop
         -- Passed cases + padding + actual pixel
         -- Checking Diagonal
         W := (X-1)* S + WH -1;
         H := (Y-1)* S + WH -1 ;
         Res := Get_Pixel(Pixels, W, H, Wh, S,  Integer(Gdk.Pixbuf.Get_Rowstride(Map)), Integer(Gdk.Pixbuf.Get_N_Channels(Map)), 0);
         if Res /= 0 then
            return Wall;
         end if;
      end loop;
      for WH in 1..S loop
         -- Passed cases + padding + actual pixel
         -- Checking Diagonal
         W := (X-1)* S + 43 - WH +1;
         H := (Y-1)* S + WH -1 ;
         Res := Get_Pixel(Pixels, W, H, Wh, S,  Integer(Gdk.Pixbuf.Get_Rowstride(Map)), Integer(Gdk.Pixbuf.Get_N_Channels(Map)),0);
         if Res /= 0 then
            return Wall;
         end if;
      end loop;
      return Empty;
   end Check_Content;

   procedure Draw_Bubble(X : in Integer; Y : in Integer; Map : in out Gdk_Pixbuf) is
      Bubble : Gdk_Pixbuf;
      Error : GError;
      Size : Gint := 43;
   begin
      Gdk_New_From_File(Bubble, "resource/bubble.png", Error);

      Gdk.Pixbuf.Copy_Area(Src_Pixbuf  => Bubble,
                           Src_X       => 0,
                           Src_Y       => 0,
                           Width       => Size,
                           Height      => Size,
                           Dest_Pixbuf => Map,
                           Dest_X      => Gint(X-1) * Size,
                           Dest_Y      => Gint(Y-1) * Size);
   end Draw_Bubble;



   function Move(Player_c : in out Player.Player; Game_space : in out Game) return
     Game_View.Content is
      Acte : Action;
      Content : Game_View.Content := Wall;
   begin
      Acte := Player_c.Orientation;

      case Acte is
         when Up =>
            Content := Game_View.Check_Content(Player_c.Pos.X,
                                               Player_c.Pos.Y + 1,
                                               Game_space.Actual_Map);
            if Player_c.Pos.Y + 1 > 28 and Content /= Wall then
               Player_c.Pos.Y := Player_c.Pos.Y + 1;
            end if;
         when Down =>
            Content := Game_View.Check_Content(Player_c.Pos.X,
                                               Player_c.Pos.Y - 1,
                                               Game_space.Actual_Map);
            if Player_c.Pos.Y - 1 < 1 and Content /= Wall then
               Player_c.Pos.Y := Player_c.Pos.Y - 1;
            end if;
         when Right =>
            Content := Game_View.Check_Content(Player_c.Pos.X + 1,
                                              Player_c.Pos.Y,
                                              Game_space.Actual_Map);
            if Player_c.Pos.X + 1 > 28 and Content /= Wall then
               Player_c.Pos.X := Player_c.Pos.X + 1;
            end if;
         when Left =>
            Content := Game_View.Check_Content(Player_c.Pos.X-1,
                                               Player_c.Pos.Y,
                                               Game_space.Actual_Map);
            if Player_c.Pos.X - 1 < 1 and Content /= Wall then
               Player_c.Pos.X := Player_c.Pos.X - 1;
            end if;
         when others =>
            null;
      end case;
      return Content;
   end Move;


   procedure Draw_Player(Player_c : in Player.Player; Map : in out Gdk_Pixbuf ) is
      Character : Gdk_Pixbuf;
      Size : Gint := 43;
      Error : GError;
   begin
      Gdk_New_From_File(Character, "resource/sprite.png", Error);


      Gdk.Pixbuf.Copy_Area(Src_Pixbuf  => Character,
                           Src_X       => 110,
                           Src_Y       => 8,
                           Width       => Size,
                           Height      => Size,
                           Dest_Pixbuf => Map,
                           Dest_X      => Gint(Player_c.Pos.X -1) * Size + Size/2,
                           Dest_Y      => Gint(Player_c.Pos.Y -1) * Size);
   end Draw_Player;
end Game_View;
